import logo from "./logo.svg";
import "./App.css";
import BaitapComponent from "./components/Baitap/BaitapComponent";

function App() {
  return (
    <div className="App">
      <BaitapComponent></BaitapComponent>
    </div>
  );
}

export default App;
