import React, { Component } from "react";
import Footer from "./Footer";
import Header from "./Header";
import Pagecontent from "./Pagecontent";
import Slider from "./Slider";

export default class BaitapComponent extends Component {
  render() {
    return (
      <div>
        <Header></Header>
        <Slider></Slider>
        <Pagecontent></Pagecontent>
        <Footer></Footer>
      </div>
    );
  }
}
